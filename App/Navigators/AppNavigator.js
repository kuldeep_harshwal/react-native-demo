/* eslint-disable prettier/prettier */
import { createAppContainer, createStackNavigator, createSwitchNavigator, createDrawerNavigator } from 'react-navigation'

import ExampleScreen from 'App/Containers/Example/ExampleScreen'
import SplashScreen from 'App/Containers/SplashScreen/SplashScreen'
import HomeScreen from 'App/Containers/HomeScreen/HomeScreen'
import ContactScreen from 'App/Containers/ContactScreen/ContactScreen'


/**
 * The root screen contains the application's navigation.
 *
 * @see https://reactnavigation.org/docs/en/hello-react-navigation.html#creating-a-stack-navigator
 */
const DrawerNav = createDrawerNavigator({
  Home: {
    screen: HomeScreen
  },
  Contact: {
    screen: ContactScreen
  },
  Example: {
    screen: ExampleScreen
  },

}, {
  drawerWidth: 300
});
// const StackNavigator = createStackNavigator(
//   {
//     // Create the application routes here (the key is the route name, the value is the target screen)
//     // See https://reactnavigation.org/docs/en/stack-navigator.html#routeconfigs
//     SplashScreen: SplashScreen,
//     // The main application screen is our "ExampleScreen". Feel free to replace it with your
//     // own screen and remove the example.
//     MainScreen: ExampleScreen,
//   },
//   {
//     // By default the application will show the splash screen
//     initialRouteName: 'SplashScreen',
//     // See https://reactnavigation.org/docs/en/stack-navigator.html#stacknavigatorconfig
//     headerMode: 'none',
//   }
// )

export const AppSwitch = createSwitchNavigator(
  {
    SplashScreen: SplashScreen,
    MainScreen: DrawerNav,
  },
  {
    initialRouteName: 'SplashScreen',
  }
)

export default createAppContainer(AppSwitch)
