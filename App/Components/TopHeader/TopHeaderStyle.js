/* eslint-disable prettier/prettier */
import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    topHeaderContainer: {
        flexDirection: 'row',
        marginBottom: 5,
        marginTop: 50,
        zIndex: 100
    },
})
