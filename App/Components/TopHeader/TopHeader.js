// @flow

import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import Style from './TopHeaderStyle'

type Props = {
};

const icons = {
//   backWhite: require('assets/imgs/back_white.png'),
};

export const TopHeader = (props: Props) => {

  return (
    <View style={Style.topHeaderContainer}>
      <TouchableOpacity
      onPress={()=> props.navigation.openDrawer()}
      >
          <Text>Menu</Text>
        
      </TouchableOpacity>
    </View>
  );
};
