import React from 'react'
import { Text, View } from 'react-native'
import styles from './SplashScreenStyle'
import type { NavigationScreenProp } from 'react-navigation';

type Props = {
  navigation: NavigationScreenProp,
};
export default class SplashScreen extends React.Component<Props> {
  componentDidMount() {
    this.props.navigation.navigate('MainScreen')
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logo}>
          {/* You will probably want to insert your logo here */}
          <Text>LOGO</Text>
        </View>
      </View>
    )
  }
}
