/* eslint-disable prettier/prettier */
import React from 'react'
import { Text, View, Image, Button } from 'react-native'
import { connect } from 'react-redux'
import Style from './HomeScreenStyle'
import { TopHeader } from '../../Components/TopHeader/TopHeader.js'


class HomeScreen extends React.Component {
    static navigationOptions = {
        drawerLabel: 'Home',
        drawerIcon: ({ tintColor }) => (
            <Image
                source={require('../../Assets/Images/TOM.png')}
                style={[Style.icon, { tintColor: tintColor }]}
            />
        ),
    };
    render() {
        return (
            <View style={Style.parentContainer}>
                <TopHeader {...this.props} />
                <View style={Style.container}>
                    <View>
                        <Text style={Style.text}>Home Page</Text>
                    </View>
                    <View>
                        <Button onPress={() => this.props.navigation.navigate("Contact")} title="GO to contact" />
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = (dispatch) => ({
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeScreen)
