/* eslint-disable prettier/prettier */
import { StyleSheet } from 'react-native'
import Fonts from 'App/Theme/Fonts'
import ApplicationStyles from 'App/Theme/ApplicationStyles'

export default StyleSheet.create({
    container: {
        ...ApplicationStyles.screen.container,
        flex: 1,
        justifyContent: 'center',
        margin: 30,
    },
    parentContainer: {
        display: 'flex',
        flexDirection: 'row',
        height: '100%'
    },
    text: {
        ...Fonts.style.normal,
        marginBottom: 5,
        textAlign: 'center',
    }
})
