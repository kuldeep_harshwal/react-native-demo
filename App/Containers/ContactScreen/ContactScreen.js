/* eslint-disable prettier/prettier */
import React from 'react'
import { Text, View, Button } from 'react-native'
import { connect } from 'react-redux'
import Style from './ContactScreenStyle'
import { TopHeader } from '../../Components/TopHeader/TopHeader.js'


class ContactScreen extends React.Component {
    static navigationOptions = {
        drawerLabel: 'Contact',
        drawerIcon: ({ tintColor }) => (
            <View></View>
        ),
    };

    render() {
        return (
            <View style={Style.parentContainer}>
                <TopHeader {...this.props} />
                <View style={Style.container}>
                    <View>
                        <Text style={Style.text}>Contact Page</Text>
                    </View>
                    <View>
                        <Button onPress={() => this.props.navigation.navigate("Home")} title="GO to Home" />
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = (dispatch) => ({
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ContactScreen)
